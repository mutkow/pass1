﻿using Microsoft.AspNetCore.Mvc;
using Pass1.Infrastructure.Commands;
using Pass1.Infrastructure.Commands.Users;
using Pass1.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pass1.Api.Controllers
{
    public class AccountController : ApiControllerBase
    {
        private readonly IJwtHandler _jwtHandler;

        public AccountController(ICommandDispatcher commandDispatcher,
            IJwtHandler jwtHandler) : base(commandDispatcher)
        {
            _jwtHandler = jwtHandler;
        }


        //[HttpGet("token")]
        //public IActionResult Get()
        //{
        //    var token = _jwtHandler.CreateToken("user1@email.com", "user");

        //    return Json(token);
        //}


        [HttpPut("password")]
        public async Task<IActionResult> Put([FromBody]ChangeUserPassword command)
        {
            await DispatchAsync(command);

            return NoContent();
        }
    }
}
