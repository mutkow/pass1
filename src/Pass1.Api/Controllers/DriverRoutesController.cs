﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pass1.Infrastructure.Commands;
using Pass1.Infrastructure.Commands.Drivers;
using Microsoft.AspNetCore.Authorization;
using Pass1.Infrastructure.Services;

namespace Pass1.Api.Controllers
{
    [Route("drivers/routes")]
    public class DriverRoutesController : ApiControllerBase
    {

        private readonly IDriverService _driverService;


        public DriverRoutesController(ICommandDispatcher commandDispatcher, 
            IDriverService driverService)
            : base(commandDispatcher)
        {
            _driverService = driverService;
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateDriverRoute command)
        {
            await DispatchAsync(command);

            return NoContent();
        }

        [Authorize]
        [HttpDelete("{name}")]
        public async Task<IActionResult> Delete(string name)
        {
            var command = new DeleteDriverRoute
            {
                Name = name
            };
            await DispatchAsync(command);

            return NoContent();
        }
    }
}
