﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pass1.Core.Domain
{
    public class Driver
    {
        private ISet<Route> _routes = new HashSet<Route>();

        public Guid UserId { get; protected set; }

        public string  Name { get; protected set; }

        public Vehicle Vehicle { get; protected set; }

        public IEnumerable<Route> Routes => _routes;


        protected Driver()
        {

        }

        public Driver(User user)
        {
            UserId = user.Id;
            Name = user.Username;
        }

        public void SetVehicle(Vehicle vehicle)
        {
            Vehicle = vehicle;
        }

        public void AddRoute(string name, Node start, Node end, double distance)
        {
            var route = Routes.SingleOrDefault(x => x.Name == name);
            if (route != null)
            {
                throw new Exception($"Route with name: '{name}' already exists for driver: {Name}.");
            }
            if(distance < 0)
            {
                throw new Exception("Route can not have negative distance");
            }
            _routes.Add(Route.Create(name, start, end, distance));
        }

        public void DeleteRoute(string name)
        {
            var route = Routes.SingleOrDefault(x => x.Name == name);
            if (route == null)
            {
                throw new Exception($"Route named: '{name}' for driver: '{Name}' was not found.");
            }
            _routes.Remove(route);
        }

    }
}
