﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Core.Domain
{
    public class User
    {
        public Guid Id { get; protected set; }

        public string Email { get; protected set; }

        public string Username { get; protected set; }

        public string FullName { get; protected set; }

        public string Password { get; protected set; }

        public string Salt { get; protected set; }

        public string Role { get; protected set; }

        public DateTime CreatedAt { get; protected set; }

        protected User()
        {
        }

        public User(Guid userId, string email, string password, string username, string salt, string role)
        {
            Id = userId;
            Email = email.ToLowerInvariant();
            SetPassword(password, salt);
            Username = username;
            Role = role;
            CreatedAt = DateTime.UtcNow;
        }

        public void SetPassword(string password, string salt)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new DomainException(ErrorCodes.InvalidPassword,"Password can not be empty");
            }
            if (password.Length > 100)
            {
                throw new DomainException(ErrorCodes.InvalidPassword, "Password can not be more then 100 characteres");
            }
            if (Password == password)
            {
                return;
            }
            Password = password;
            Salt = salt;
        }

        public void SetUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                throw new DomainException(ErrorCodes.InvalidUsername,
                    "Username is invalid.");
            }

            if (String.IsNullOrEmpty(username))
            {
                throw new DomainException(ErrorCodes.InvalidUsername,
                    "Username is invalid.");
            }

            Username = username.ToLowerInvariant();
        }

        public void SetEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new DomainException(ErrorCodes.InvalidEmail,
                    "Email can not be empty.");
            }
            if (Email == email)
            {
                return;
            }

            Email = email.ToLowerInvariant();
        }

    }
}
