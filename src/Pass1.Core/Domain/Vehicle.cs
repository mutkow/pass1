﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Core.Domain
{
    public class Vehicle
    {
        public string Brand { get; protected set; }

        public string Name { get; protected set; }

        public int Seats { get; protected set; }

        protected Vehicle()
        {

        }

        private Vehicle(string brand, string name, int seats)
        {
            SetBrand(brand);
            SetName(name);
            SetSeats(seats);
        }

        private void SetBrand(string brand)
        {
            if (string.IsNullOrWhiteSpace(brand))
            {
                throw new Exception("Please provide valida data");
            }
            if (Brand == brand)
            {
                return;
            }
            Brand = brand;
        }

        private void SetName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("Please provide valida data");
            }
            if (Name == name)
            {
                return;
            }
            Name = name;
        }

        private void SetSeats(int seats)
        {
            if (seats < 0)
            {
                throw new Exception("Please provide valida data");
            }
            if (Seats == seats)
            {
                return;
            }
            Seats = seats;
        }

        public  static Vehicle Create(string name, string brand, int seats)
        {
            return new Vehicle(brand, name, seats);
        }
    }
}
