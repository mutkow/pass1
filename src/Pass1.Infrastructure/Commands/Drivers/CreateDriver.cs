﻿using Pass1.Infrastructure.Commands.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Commands.Drivers
{
    public class CreateDriver : AuthenticatedCommandBase
    {
        public DriverVehicle Vehicle { get; set; }
    }
}
