﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Commands.Drivers
{
    public class DeleteDriverRoute  : AuthenticatedCommandBase
    {
        public Guid UserId { get; set; }

        public string Name { get; set; }
    }
}
