﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Commands.Drivers.Models
{
    public class DriverVehicle
    {
        public string Brand { get; set; }

        public string Name { get; set; }
    }
}
