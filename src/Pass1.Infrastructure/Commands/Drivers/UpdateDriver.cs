﻿using Pass1.Infrastructure.Commands.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Commands.Drivers
{
    public class UpdateDriver : AuthenticatedCommandBase
    {
        public DriverVehicle Vehicle { get; set; }
    }
}
