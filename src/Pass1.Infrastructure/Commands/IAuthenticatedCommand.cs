﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Commands
{
    public interface IAuthenticatedCommand : ICommand
    {
        Guid UserId { get; set; }
    }
}
