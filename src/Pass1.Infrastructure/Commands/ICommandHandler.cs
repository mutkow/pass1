﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Commands
{
    public interface ICommandHandler<T> where T : ICommand
    {
        Task HadleAsync(T command);
    }
}
