﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.DTO
{
    public class DriverDetailsDto : DriverDto
    {
        public IEnumerable<RouteDto> Routes { get; set; } 
    }
}
