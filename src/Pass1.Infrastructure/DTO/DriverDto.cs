﻿using Pass1.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.DTO
{
    public class DriverDto
    {
        public Guid UserId { get; set; }

        public string Name { get;  set; }

        public VehicleDto Vehicle { get; set; }
    }
}
