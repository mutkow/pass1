﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.DTO
{
    public class NodeDto
    {
        public string Address { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
