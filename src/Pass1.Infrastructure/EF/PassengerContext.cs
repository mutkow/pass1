﻿using Microsoft.EntityFrameworkCore;
using Pass1.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.EF
{
    public class PassengerContext : DbContext
    {
        private readonly SqlSettings _sqlSettings;

        public DbSet<User> Users { get; set; }

        public PassengerContext(DbContextOptions<PassengerContext> options, SqlSettings sqlsettings) : base(options)
        {
            _sqlSettings = sqlsettings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            if(_sqlSettings.InMemory)
            {
                optionBuilder.UseInMemoryDatabase();
                return;
            }
            optionBuilder.UseSqlServer(_sqlSettings.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var userBuilder = modelBuilder.Entity<User>();
            userBuilder.HasKey(x => x.Id);
        }
    }
}
