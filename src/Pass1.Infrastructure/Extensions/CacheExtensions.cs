﻿using Microsoft.Extensions.Caching.Memory;
using Pass1.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Extensions
{
    public static class CacheExtensions
    {
        public static void SetJwt(this IMemoryCache cache, Guid tokenId, JwtDto jwt)
        {
             cache.Set(GetJwtKey(tokenId), jwt, TimeSpan.FromSeconds(5));
        }

        public static JwtDto GetJwt(this IMemoryCache cache, Guid tokenId)
        {
            return cache.Get<JwtDto>(GetJwtKey(tokenId));
        }

        private static string GetJwtKey(Guid tokenId)
        {
            return $"jwt-{tokenId}";
        }
    }
}
