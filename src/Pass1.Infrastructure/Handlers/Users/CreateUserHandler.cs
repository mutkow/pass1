﻿using Pass1.Infrastructure.Commands;
using Pass1.Infrastructure.Commands.Users;
using Pass1.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Handlers.Users
{
    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly IUserService _userService;
        
        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
        }


        public async Task HadleAsync(CreateUser command)
        {
            await _userService.RegisterAsync(command.Id, command.Email, command.Username, command.Password, command.Role);
        }
    }
}
