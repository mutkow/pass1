﻿using Microsoft.Extensions.Caching.Memory;
using Pass1.Infrastructure.Commands;
using Pass1.Infrastructure.Commands.Users;
using Pass1.Infrastructure.Extensions;
using Pass1.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Handlers.Users
{
    public class LoginHandler : ICommandHandler<Login>
    {
        private readonly IUserService _userService;

        private readonly IJwtHandler _jwtHandler;

        private readonly IMemoryCache _cache;

        private readonly IHandler _handler;

        public LoginHandler(IHandler handler, IUserService userService, JwtHandler jwtHandler, IMemoryCache cache)
        {
            _userService = userService;
            _jwtHandler = jwtHandler;
            _cache = cache;
            _handler = handler;
        }

        public async Task HadleAsync(Login command)
        {
            await _handler.Run(async () => await _userService.LoginAsync(command.Email, command.Password))
            .Next()
            .Run(async () =>
            {
                var user = await _userService.GetAsync(command.Email);
                var jwt = _jwtHandler.CreateToken(user.Id, user.Role);
                _cache.SetJwt(command.TokenId, jwt);
            })
            .ExecuteAsync();
        }

        //public async Task HadleAsync(Login command)
        //{
        //    await _userService.LoginAsync(command.Email, command.Password);
        //    var user = await _userService.GetAsync(command.Email);
        //    var jwt = _jwtHandler.CreateToken(user.Id, user.Role);
        //    _cache.SetJwt(command.TokenId, jwt);
        //}
    }
}
