﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Pass1.Infrastructure.EF;
using Pass1.Infrastructure.Extensions;
using Pass1.Infrastructure.Mongo;
using Pass1.Infrastructure.Settings;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Pass1.Infrastructure.IoC.Modules
{
    public class SettingsModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public SettingsModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterInstance(_configuration.GetSettings<GeneralSettings>())
                .SingleInstance();

            builder.RegisterInstance(_configuration.GetSettings<JwtSettings>())
                .SingleInstance();

            builder.RegisterInstance(_configuration.GetSettings<MongoSettings>())
             .SingleInstance();

            builder.RegisterInstance(_configuration.GetSettings<SqlSettings>())
 .SingleInstance();
        }
    }
}
