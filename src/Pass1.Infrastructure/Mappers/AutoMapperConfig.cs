﻿using AutoMapper;
using Pass1.Core.Domain;
using Pass1.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Mappers
{
    public static class AutoMapperConfig
    {
        public static IMapper Initialize()
         => new MapperConfiguration(cfg =>
         {
             cfg.CreateMap<User, UserDto>();
             cfg.CreateMap<Driver, DriverDto>();
             cfg.CreateMap<Driver, DriverDetailsDto>();
             cfg.CreateMap<Route, RouteDto>();
             cfg.CreateMap<Vehicle, VehicleDto>();
             cfg.CreateMap<Node, NodeDto>();
         })
         .CreateMapper();
    }
}
