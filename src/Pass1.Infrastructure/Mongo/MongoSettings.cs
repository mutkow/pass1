﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Mongo
{
    public class MongoSettings
    {
        public string ConnectionString { get; set; }

        public string DataBase { get; set; }
    }
}
