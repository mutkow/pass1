﻿using Pass1.Core.Domain;
using Pass1.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Repositories
{
    public class DriverRepository : IDriverRepository
    {
        private static ISet<Driver> _drivers = new HashSet<Driver>();


        public async Task<Driver> GetAsync(Guid userId)
        {
            return await Task.FromResult(_drivers.SingleOrDefault(x => x.UserId == userId));
        }

        public async Task<IEnumerable<Driver>> GetAllAsync()
        {
            return await Task.FromResult(_drivers);
        }

        public async Task AddAsync(Driver driver)
        {
            _drivers.Add(driver);
            await Task.CompletedTask;
        }

        public async Task UpdateAsync(Driver driver)
        {
            await Task.CompletedTask;
        }

        public async Task DeleteAsync(Driver driver)
        {
            _drivers.Remove(driver);
            await Task.CompletedTask;
        }
    }
}
