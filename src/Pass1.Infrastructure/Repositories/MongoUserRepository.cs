﻿using MongoDB.Driver;
using Pass1.Core.Domain;
using Pass1.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Repositories
{
    public class MongoUserRepository : IUserRepository, IMongoRepository
    {
        private readonly IMongoDatabase _database;

        public MongoUserRepository(IMongoDatabase database)
        {
            _database = database;
        }

        public async Task<User> GetAsync(Guid id)
        {
            return await Users.AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User> GetAsync(string email)
        {
            return await Users.AsQueryable().FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<IEnumerable<User>> BrowseAsync()
        {
            return await Users.AsQueryable().ToListAsync();
        }

        public async Task AddAsync(User user)
        {
            await Users.InsertOneAsync(user);
        }

        public async Task RemoveAsync(Guid id)
        {
            await Users.DeleteOneAsync(x => x.Id == id);
        }

        public async Task UdpateAsync(User user)
        {
            await Users.ReplaceOneAsync(x => x.Id == user.Id, user);
        }

        private IMongoCollection<User> Users => _database.GetCollection<User>("Users");
    }
}
