﻿using Pass1.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Pass1.Core.Domain;
using System.Threading.Tasks;
using Pass1.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;

namespace Pass1.Infrastructure.Repositories
{
    public class SqlUserRepository : IUserRepository, ISqlRepository
    { 
        private readonly PassengerContext _context;

        public SqlUserRepository(PassengerContext context)
        {
            _context = context;
        }

        public async Task<User> GetAsync(Guid id)
        {
            return await _context.Users.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<User> GetAsync(string email)
        {
            return await _context.Users.SingleOrDefaultAsync(x => x.Email == email);
        }

        public async Task<IEnumerable<User>> BrowseAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task AddAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }

        public async Task UdpateAsync(User user)
        {
             _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var user = await GetAsync(id);
             _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

    }
}
