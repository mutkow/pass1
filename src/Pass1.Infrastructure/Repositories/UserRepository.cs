﻿using Pass1.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Pass1.Core.Domain;
using System.Threading.Tasks;
using System.Linq;

namespace Pass1.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private static ISet<User> _users = new HashSet<User>()
        {
            //new User("user1@user1", "password","user1","salt", "user")
        };

        public async Task<User> GetAsync(Guid id)
        {
            return await Task.FromResult(_users.SingleOrDefault(x => x.Id == id));
        }

        public async Task<User> GetAsync(string email)
        {
            return await Task.FromResult(_users.SingleOrDefault(x => x.Email == email.ToLowerInvariant()));
        }

        public async Task<IEnumerable<User>> BrowseAsync()
        {
            return await Task.FromResult(_users);
        }

        public async Task AddAsync(User user)
        {
            _users.Add(user);
            await Task.CompletedTask;
        }

        public async Task RemoveAsync(Guid id)
        {
            var user = await GetAsync(id);
            _users.Remove(user);
            await Task.CompletedTask;
        }

        public async Task UdpateAsync(User user)
        {
            await Task.CompletedTask;
        }
    }
}
