﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Services
{
    public class DataInitializer : IDataInitializer
    {
        private readonly IUserService _userService;

        private readonly ILogger<DataInitializer> _logger;

        private readonly IDriverService _driverService;

        private readonly IDriverRouteService _driverRouteService;

        public DataInitializer(IUserService userService, ILogger<DataInitializer> logger, IDriverService driverService,
            IDriverRouteService driverRouteService)
        {
            _userService = userService;
            _logger = logger;
            _driverService = driverService;
            _driverRouteService = driverRouteService;
        }

        public async Task SeedAsync()
        {
            var users = await _userService.BrowseAsync();
            if(users.Any())
            {
                return;
            }
            _logger.LogTrace("!!!!!!!!!!!!!!!1Initializing data......");
            var tasks = new List<Task>();
            for (var i=1; i<=10; i++)
            {
                var userId = Guid.NewGuid();
                var username = $"user{i}";
                await (_userService.RegisterAsync(userId, $"{username}@test.com", username, "secret", "user"));
                await (_driverService.CreateAsync(userId));
                await (_driverService.SetVehicleAsync(userId, "BMW", "i8"));
                _logger.LogTrace($"!!!!!!!!!!!!!!!Adding route for {username}");
                await (_driverRouteService.AddAsync(userId, "Default Route", 1, 1, 2, 2));
                await (_driverRouteService.AddAsync(userId, "Work Route", 3, 4, 12, 13));
            }
            for (var i=1; i <=3; i++)
            {
                var userId = Guid.NewGuid();
                var username = $"admin{i}";
                await (_userService.RegisterAsync(userId, $"{username}@test.com", username, "secret", "admin"));
            }
            await Task.WhenAll(tasks);
            _logger.LogTrace("!!!!!!!!!!!!!Data was initialized......");
        }
    }
}
