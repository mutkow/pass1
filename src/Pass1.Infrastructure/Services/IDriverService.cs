﻿using Pass1.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Services
{
    public interface IDriverService : IService
    {
        Task<DriverDetailsDto> GetAsync(Guid userId);

        Task<IEnumerable<DriverDto>> BrowseAsync();

        Task CreateAsync(Guid userId);

        Task SetVehicleAsync(Guid userId, string brand, string name);

        Task DeleteAsync(Guid userId);

    }
}
