﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Services
{
    public interface IHandler : IService
    {
        IHandlerTask Run(Func<Task> run);

        IHandlerTaskRunner Validate(Func<Task> validate);

        Task ExecuteAllAsync();
    }
}
