﻿using Pass1.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Services
{
    public interface IHandlerTask
    {
        IHandlerTask Always(Func<Task> always);

        IHandlerTask OnCustomError(Func<PassengerException, Task> onCustomError,
            bool propagateException = false, bool executeOnError = false);

        IHandlerTask OnError(Func<Exception, Task> onError,
             bool propagateException = false);

        IHandlerTask OnSuccess(Func<Task> onSuccess);

        IHandlerTask PropagateException();

        IHandlerTask DoNotPropagateException();

        IHandler Next();

        Task ExecuteAsync();
    }
}
