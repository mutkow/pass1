﻿using Pass1.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Services
{
    public interface IJwtHandler
    {
        JwtDto CreateToken(Guid userId, string role);
    }
}
