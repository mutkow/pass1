﻿using Pass1.Core.Domain;
using Pass1.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pass1.Infrastructure.Services
{
    public interface IVehicleProvider : IService
    {
        Task<IEnumerable<VehicleDto>> BrowseAsync();

        Task<VehicleDto> GetAsync(string brand, string name);
    }
}
