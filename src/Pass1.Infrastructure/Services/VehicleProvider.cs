﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Pass1.Core.Domain;
using Pass1.Infrastructure.DTO;
using Microsoft.Extensions.Caching.Memory;
using System.Linq;

namespace Pass1.Infrastructure.Services
{
    public class VehicleProvider : IVehicleProvider
    {
        private readonly IMemoryCache _cache;

        private readonly static string CacheKey = "vehicles";

        private static readonly IDictionary<string, IEnumerable<VehicleDetails>> availableVehicles =
            new Dictionary<string, IEnumerable<VehicleDetails>>
            {
                ["Audi"] = new List<VehicleDetails>
                {
                    new VehicleDetails("RSA",5)
                },
                ["BMW"] = new List<VehicleDetails>
                {
                    new VehicleDetails("i8",3),
                    new VehicleDetails("E36",5)
                },
                ["Ford"] = new List<VehicleDetails>
                {
                    new VehicleDetails("Fiesta",5)
                },
            };



        public VehicleProvider(IMemoryCache cache)
        {
            _cache = cache;
        }

        public async Task<VehicleDto> GetAsync(string brand, string name)
        {
            if(!availableVehicles.ContainsKey(brand))
            {
                throw new Exception("Vehicle brand is not available");
            }
            var vehicles = availableVehicles[brand];
            var vehicle = vehicles.SingleOrDefault(x => x.Name == name);
            if(vehicle == null)
            {
                throw new Exception("Vehicles is not avaiable");
            }
            return await Task.FromResult(new VehicleDto
            {
                Brand = brand,
                Name = vehicle.Name,
                Seats = vehicle.Seats
            });
        }

        public async Task<IEnumerable<VehicleDto>> BrowseAsync()
        {
            var vehicles = _cache.Get<IEnumerable<VehicleDto>>(CacheKey);
            if (vehicles == null)
            {
                vehicles = await GetAllAsync();
                _cache.Set(CacheKey, vehicles);
                Console.WriteLine("Getting vechicles from Database");
            }
            else
            {
                Console.WriteLine("Getting vechicles from cache");
            }
            return vehicles;
        }

        public async Task<IEnumerable<VehicleDto>> GetAllAsync()
        {
            return await Task.FromResult(availableVehicles.GroupBy(x => x.Key)
                .SelectMany(g => g.SelectMany(v => v.Value.Select(c => new VehicleDto
                {
                    Brand = v.Key,
                    Name = c.Name,
                    Seats = c.Seats
                }))));
        }

        private class VehicleDetails
        {
            public string Name { get; }

            public int Seats { get; }

            public VehicleDetails(string name, int seats)
            {
                Name = name;
                Seats = seats;
            }
        }
    }
}
