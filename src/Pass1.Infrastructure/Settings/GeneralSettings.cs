﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pass1.Infrastructure.Settings
{
    public class GeneralSettings
    {
        public string Name { get; set; }

        public bool SeedData { get; set; }
    }
}
