﻿using FluentAssertions;
using Pass1.Infrastructure.Commands.Users;
using System;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Pass1.Tests.EndToEnd.Controllers
{
    public class AccountControllerTests : ControllerTestBase
    {
        //Become
        [Fact]
        public async Task given_valid_current_and_new_password_it_should_be_changed()
        {
            // Act
            var command = new ChangeUserPassword
            {
                CurrentPassword = "secret",
                NewPassword = "secret2"
            };

            var payload = GetPayload(command);
            var response = await Client.PutAsync("account/password", payload);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NoContent);
        }

    }
}
