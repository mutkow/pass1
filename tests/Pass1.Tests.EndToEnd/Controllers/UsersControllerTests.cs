﻿using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Pass1.Api;
using Pass1.Infrastructure.Commands.Users;
using Pass1.Infrastructure.DTO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace Pass1.Tests.EndToEnd.Controllers
{
    public class UsersControllerTests : ControllerTestBase
    {
        //Become
        [Fact]
        public async Task given_valid_email_user_should_exist()
        {
            var email = "admin1@test.com";
            var user = await GetUserAsync(email);
            user.Email.ShouldBeEquivalentTo(email);
        }


        //Become
        [Fact]
        public async Task given_invalid_email_user_should_not_exist()
        {
            var email = "user123@user1";
            var response = await Client.GetAsync($"users/{email}");
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NotFound);
        }


        //Become
        [Fact]
        public async Task given_valid_unique_email_user_should_be_created()
        {
            // Act
            var command = new CreateUser
            {
                Email = "user1223@user1",
                Username = "test",
                Password = "password"
            };
            var payload = GetPayload(command);
            var response = await Client.PostAsync("users", payload);
            response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Created);
            response.Headers.Location.ToString().ShouldBeEquivalentTo($"users/{command.Email}");

            var user = await GetUserAsync(command.Email);
            user.Email.ShouldBeEquivalentTo(command.Email);
        }

        //Become
        private async Task<UserDto> GetUserAsync(string email)
        {
            var response = await Client.GetAsync($"users/{email}");
            var responseString = await response.Content.ReadAsStringAsync();
            return  JsonConvert.DeserializeObject<UserDto>(responseString);
        }
    }
}
