﻿using Pass1.Core.Domain;
using Pass1.Core.Repositories;
using Pass1.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pass1.Tests.Repositories
{
    public class UserRepositoryTests
    {
        //Strefa
        [Fact]
        public async Task when_adding_new_user_it_should_be_added_correctly_to_the_list()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "user11@test", "test", "password", "salt", "admin");
            IUserRepository repository = new UserRepository();

            //Act
            await repository.AddAsync(user);

            //Asser
            var existingUser = await repository.GetAsync(user.Email);
            Assert.Equal(user, existingUser);
        }
    }
}
