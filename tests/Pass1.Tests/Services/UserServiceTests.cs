﻿using AutoMapper;
using FluentAssertions;
using Moq;
using Pass1.Core.Domain;
using Pass1.Core.Repositories;
using Pass1.Infrastructure.DTO;
using Pass1.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pass1.Tests.Services
{
    public class UserServiceTests
    {
        //Strefa Become
        [Fact]
        public async Task register_async_should_invoke_add_assync_on_user_repository()
        {
            //Arrange
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            var encrypterMock = new Mock<IEncrypter>();
            encrypterMock.Setup(x => x.GetSalt(It.IsAny<string>())).Returns("hash");
            encrypterMock.Setup(x => x.GetHash(It.IsAny<string>(), It.IsAny<string>())).Returns("salt");

            var userService = new UserService(userRepositoryMock.Object, mapperMock.Object, encrypterMock.Object);

            //Act
            await userService.RegisterAsync(Guid.NewGuid(), "user1@user1", "user1", "passwfdfdd", "user");

            //Assert
            userRepositoryMock.Verify(x => x.AddAsync(It.IsAny<User>()), Times.Once());
        }

        //Strefa
        [Fact]
        public async Task when_invking_get_async_it_should_invoke_user_repository_getasync()
        {
            //Arrange
            var user = new User(Guid.NewGuid(), "test11@test", "password", "test11", "salt", "user");
            var userDto = new UserDto
            {
                Id = user.Id,
                Email = user.Email,
                Username = user.Username
            };
            var userRepositoryMock = new Mock<IUserRepository>();
            var mapperMock = new Mock<IMapper>();
            var encrypterMock = new Mock<IEncrypter>();

            var userService = new UserService(userRepositoryMock.Object, mapperMock.Object, encrypterMock.Object);

            userRepositoryMock.Setup(x => x.GetAsync(user.Email)).ReturnsAsync(user);
            mapperMock.Setup(x => x.Map<User, UserDto>(user)).Returns(userDto);

            //Act
            var existingUserDto = await userService.GetAsync(user.Email);

            //Assert
            userRepositoryMock.Verify(x => x.GetAsync(user.Email), Times.Once());
            existingUserDto.Should().NotBeNull();
            existingUserDto.Email.ShouldBeEquivalentTo(user.Email);
        }
    }
}
